package observer;

public class GrabStocks {
    public static void main(String[] args) {
        StockGrabber stockGrabber = new StockGrabber();

        // observer 1
        Observer observer1 = new StockObserver(stockGrabber);

        stockGrabber.setIbmPrice(197.00);
        stockGrabber.setApplePrice(2222.00);
        stockGrabber.setGooglePrice(397.00);

        // observer 2
        Observer observer2 = new StockObserver(stockGrabber);

        stockGrabber.setIbmPrice(2.00);
        stockGrabber.setApplePrice(3.00);
        stockGrabber.setGooglePrice(4.00);

        // observer 1 unregistered
        stockGrabber.unregister(observer1);


        stockGrabber.setIbmPrice(31231.00);
        stockGrabber.setApplePrice(545.00);
        stockGrabber.setGooglePrice(6452.00);

        Runnable getIBM = new GetTheStock(stockGrabber, 2, "IBM", 197.00);
        Runnable getApple = new GetTheStock(stockGrabber, 2, "APPLE", 677.00);
        Runnable getGoogle = new GetTheStock(stockGrabber, 2, "GOOGLE", 676.00);

        new Thread(getIBM).start();
        new Thread(getApple).start();
        new Thread(getGoogle).start();

    }
}
