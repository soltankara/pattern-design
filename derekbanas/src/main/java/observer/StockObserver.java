package observer;

public class StockObserver implements Observer {

    private double ibmPrice;
    private double applePrice;
    private double googlePrice;

    private static int observerIDTracker = 0;

    private int observerID;

    private Subject stockGrabber;

    public StockObserver(Subject stockGrabber) {
        this.stockGrabber = stockGrabber;
        this.observerID = ++observerIDTracker;

        System.out.println("New Stock Observer: " + this.observerID);

        stockGrabber.register(this);
    }

    @Override
    public void update(double ibmPrice, double applePrice, double googlePrice) {
        this.ibmPrice = ibmPrice;
        this.applePrice = applePrice;
        this.googlePrice = googlePrice;

        this.printPrices();
    }

    private void printPrices() {
        System.out.println("---------start---------");
        System.out.println("Observer ID: " + this.observerID);
        System.out.println("IBM Price: " + this.ibmPrice);
        System.out.println("Apple Price: " + this.applePrice);
        System.out.println("Google Price: " + this.googlePrice);
        System.out.println("");
    }
}
