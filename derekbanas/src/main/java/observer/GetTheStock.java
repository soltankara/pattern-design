package observer;

import java.text.DecimalFormat;

public class GetTheStock implements Runnable {

    private int startTime;
    private String stockName;
    private double stockPrice;

    private Subject stockGrabber;

    public GetTheStock(Subject stockGrabber, int newStartTime, String newStockName, double newStockPrice) {
        this.stockGrabber = stockGrabber;
        this.startTime = newStartTime;
        this.stockName = newStockName;
        this.stockPrice = newStockPrice;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 3; i++) {
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
            }

            double randNum = (Math.random() * 0.6) - 0.3;

            DecimalFormat df = new DecimalFormat("#.##");

            this.stockPrice = Double.valueOf(df.format((this.stockPrice + randNum)));

            if (this.stockName.equalsIgnoreCase("IBM"))
                ((StockGrabber) this.stockGrabber).setIbmPrice(this.stockPrice);

            if (this.stockName.equalsIgnoreCase("APPLE"))
                ((StockGrabber) this.stockGrabber).setApplePrice(this.stockPrice);

            if (this.stockName.equalsIgnoreCase("GOOGLE"))
                ((StockGrabber) this.stockGrabber).setGooglePrice(this.stockPrice);

            System.out.println(this.stockName + " : " + df.format((this.stockPrice + randNum)) + " " + df.format(randNum));

            System.out.println();

        }
    }
}
