package builder;

public interface RobotBuilder {
    void builRobotHead();

    void builRobotTorso();

    void builRobotArms();

    void builRobotLegs();

    Robot getRobot();
}
