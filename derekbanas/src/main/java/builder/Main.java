package builder;

public class Main {
    public static void main(String[] args) {
        RobotBuilder old = new OldRobotBuilder();
        RobotEngineer engineer = new RobotEngineer(old);
        engineer.build();
        Robot robot = engineer.getRobot();
        System.out.println("Head - " + robot.getRobotHead());
        System.out.println("Torso - " + robot.getRobotTorso());
        System.out.println("Arms - " + robot.getRobotArms());
        System.out.println("Legs - " + robot.getRobotLegs());
    }
}
