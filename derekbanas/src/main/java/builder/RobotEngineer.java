package builder;

public class RobotEngineer {
    // Director

    private RobotBuilder builder;

    public RobotEngineer(RobotBuilder builder) {
        this.builder = builder;
    }

    public Robot getRobot() {
        return this.builder.getRobot();
    }

    public void build() {
        this.builder.builRobotHead();
        this.builder.builRobotTorso();
        this.builder.builRobotArms();
        this.builder.builRobotLegs();
    }
}
