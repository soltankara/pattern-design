package builder;

public class OldRobotBuilder implements RobotBuilder {

    private Robot robot;

    public OldRobotBuilder() {
        this.robot = new Robot();
    }

    @Override
    public void builRobotHead() {
        this.robot.setRobotHead("Robot Head");
    }

    @Override
    public void builRobotTorso() {
        this.robot.setRobotTorso("Robot Torso");
    }

    @Override
    public void builRobotArms() {
        this.robot.setRobotArms("Robot Arms");
    }

    @Override
    public void builRobotLegs() {
        this.robot.setRobotLegs("Robot Legs");
    }

    @Override
    public Robot getRobot() {
        return this.robot;
    }
}
