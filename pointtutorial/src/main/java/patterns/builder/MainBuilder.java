package patterns.builder;

import patterns.builder.meal.Meal;
import patterns.builder.meal.MealBuilder;

public class MainBuilder {
    public static void main(String[] args) {
        MealBuilder builder = new MealBuilder();

        Meal meal = builder.prepareVegMeal();
        System.out.println("Vegetarian Meal");
        meal.showItems();
        System.out.println("Total Cost: " + meal.getCost());

        System.out.println("---------------------------");

        meal = builder.prepareNonVegMeal();
        System.out.println("Non Vegetarian Meal");
        meal.showItems();
        System.out.println("Total Cost: " + meal.getCost());

    }
}
