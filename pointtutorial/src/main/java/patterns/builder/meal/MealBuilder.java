package patterns.builder.meal;

import patterns.builder.item.burger.ChickenBurger;
import patterns.builder.item.burger.VegBurger;
import patterns.builder.item.drink.Coke;
import patterns.builder.item.drink.Pepsi;

public class MealBuilder {

    public Meal prepareVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Coke());

        return meal;
    }

    public Meal prepareNonVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new ChickenBurger());
        meal.addItem(new Pepsi());
        return meal;
    }
}
