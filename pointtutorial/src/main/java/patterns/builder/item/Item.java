package patterns.builder.item;

import patterns.builder.packing.Packing;

public interface Item {
    public String name();
    public Packing packing();
    public float price();
}
