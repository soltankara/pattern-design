package patterns.builder.item.drink;

import patterns.builder.item.drink.ColdDrink;

public class Pepsi extends ColdDrink {
    @Override
    public String name() {
        return "Pepsi";
    }

    @Override
    public float price() {
        return 30.22f;
    }
}
