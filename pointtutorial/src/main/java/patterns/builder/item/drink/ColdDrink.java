package patterns.builder.item.drink;

import patterns.builder.item.Item;
import patterns.builder.packing.Bottle;
import patterns.builder.packing.Packing;

public abstract class ColdDrink implements Item {

    @Override
    public Packing packing() {
        return new Bottle();
    }

    @Override
    public abstract float price();
}
