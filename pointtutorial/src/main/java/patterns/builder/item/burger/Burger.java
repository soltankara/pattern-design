package patterns.builder.item.burger;

import patterns.builder.item.Item;
import patterns.builder.packing.Packing;
import patterns.builder.packing.Wrapper;

public abstract class Burger implements Item {

    @Override
    public Packing packing() {
        return new Wrapper();
    }

    @Override
    public abstract float price();
}
