package patterns.observer;

public class Main {

    public static void main(String[] args) {
        Subject subject = new Subject();

        new BinaryObserver(subject);
        new OctalObserver(subject);
        new HexaObserver(subject);

        System.out.println("First state change is 15");
        subject.setState(15);
        System.out.println("First state change is 10");
        subject.setState(10);

    }
}
