package patterns.singleton;

public class MainSingleton {
    public static void main(String[] args) {
        Singleton instane = Singleton.getInstance();
        instane.showMessage();
    }
}
