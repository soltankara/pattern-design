package patterns.facade.shape;

public interface Shape {

    void draw();
}
