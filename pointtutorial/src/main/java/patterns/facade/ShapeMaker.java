package patterns.facade;

import patterns.facade.shape.Circle;
import patterns.facade.shape.Rectangle;
import patterns.facade.shape.Shape;
import patterns.facade.shape.Square;

public class ShapeMaker {

    private Shape circle;
    private Shape square;
    private Shape rectangle;

    public ShapeMaker() {
        this.circle = new Circle();
        this.square = new Square();
        this.rectangle = new Rectangle();
    }

    public void drawCircle() {
        circle.draw();
    }

    public void drawSquare() {
        square.draw();
    }

    public void drawRectangle() {
        rectangle.draw();
    }
}
