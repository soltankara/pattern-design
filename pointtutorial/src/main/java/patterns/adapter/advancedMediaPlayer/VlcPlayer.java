package patterns.adapter.advancedMediaPlayer;

public class VlcPlayer implements AdvancedMediaPlayer {

    @Override
    public void playVlc(String fileName) {
        System.out.println("Playing VLC - file name: " + fileName);
    }

    @Override
    public void playMp4(String fileName) {
        // todo nothing
    }
}
