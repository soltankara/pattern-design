package patterns.adapter.advancedMediaPlayer;

public class Mp4Player implements AdvancedMediaPlayer {

    @Override
    public void playVlc(String fileName) {
        // todo nothing
    }

    @Override
    public void playMp4(String fileName) {
        System.out.println("MP4 Playing - file name: " + fileName);
    }
}
