package patterns.factory;

import patterns.factory.shape.Circle;
import patterns.factory.shape.Rectangle;
import patterns.factory.shape.Shape;
import patterns.factory.shape.Square;

public class ShapeFactory {

    public Shape getShape(String shapeType) {
        if (shapeType == null) return null;

        if (shapeType.equalsIgnoreCase("CIRCLE")) {
            return new Circle();
        } else if (shapeType.equalsIgnoreCase("SQUARE")) {
            return new Square();
        } else if (shapeType.equalsIgnoreCase("RECTANGLE")) {
            return new Rectangle();
        }

        return null;
    }
}
