package patterns.factory.shape;

public interface Shape {
    void draw();
}
