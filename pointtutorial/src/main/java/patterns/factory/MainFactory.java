package patterns.factory;

import patterns.factory.shape.Shape;

public class MainFactory {
    public static void main(String[] args) {
        ShapeFactory factory = new ShapeFactory();

        Shape shape = factory.getShape("CIRCLE");
        shape.draw();

        shape = factory.getShape("SQUARE");
        shape.draw();

        shape = factory.getShape("RECTANGLE");
        shape.draw();
    }
}
