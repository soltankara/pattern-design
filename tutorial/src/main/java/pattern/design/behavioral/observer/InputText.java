package pattern.design.behavioral.observer;

public class InputText implements Observer {

    public void setText(String text) {
        System.out.println("Input Text is: " + text);
    }

    @Override
    public void update() {
        setText("Input text button has been clicked!");
    }
}
