package pattern.design.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class Button implements Subject {

    private List<Observer> observers = new ArrayList<Observer>();

    public void clicked() {
        change();
    }

    @Override
    public void attachObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void detachObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void change() {
        for (Observer observer : this.observers) {
            observer.update();
        }
    }
}
