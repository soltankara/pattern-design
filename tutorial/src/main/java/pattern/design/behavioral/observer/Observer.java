package pattern.design.behavioral.observer;

public interface Observer {
    void update();
}
