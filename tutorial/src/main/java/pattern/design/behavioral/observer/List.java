package pattern.design.behavioral.observer;

public class List implements Observer {

    public void setListValue(int value) {
        System.out.println("List value: " + value);
    }

    @Override
    public void update() {
        setListValue(1);
    }
}
