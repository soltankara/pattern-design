package pattern.design.behavioral.strategy.algorithm;

public class HardChessAlgorithm implements ChessAlgorithm {

    @Override
    public Integer calculateNextStep() {
        return 3;
    }
}
