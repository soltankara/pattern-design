package pattern.design.behavioral.strategy;

import pattern.design.behavioral.strategy.algorithm.ChessAlgorithm;
import pattern.design.behavioral.strategy.algorithm.HardChessAlgorithm;

public class Strategy {
    public static void main(String[] args) {
        Chess chess = new Chess();
        System.out.println("Calculate chess level: " + chess.calcNextStep());

        ChessAlgorithm algorithm = new HardChessAlgorithm();
        chess.setAlgorithm(algorithm);
        System.out.println("Calculate chess level: " + chess.calcNextStep());
    }
}
