package pattern.design.behavioral.strategy.algorithm;

public class EasyChessAlgorithm implements ChessAlgorithm{

    @Override
    public Integer calculateNextStep() {
        return 1;
    }
}
