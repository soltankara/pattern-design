package pattern.design.behavioral.strategy.algorithm;

public class MediumChessAlgorithm implements ChessAlgorithm {

    @Override
    public Integer calculateNextStep() {
        return 2;
    }
}
