package pattern.design.behavioral.strategy.algorithm;

public interface ChessAlgorithm {

    Integer calculateNextStep();

}
