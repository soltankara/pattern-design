package pattern.design.behavioral.strategy;

import pattern.design.behavioral.strategy.algorithm.ChessAlgorithm;
import pattern.design.behavioral.strategy.algorithm.EasyChessAlgorithm;

public class Chess {

    private ChessAlgorithm algorithm = new EasyChessAlgorithm();

    public Integer calcNextStep() {
        return algorithm.calculateNextStep();
    }

    public void setAlgorithm(ChessAlgorithm algorithm) {
        this.algorithm = algorithm;
    }
}
