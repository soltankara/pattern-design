package pattern.design.creational.builder;

import pattern.design.creational.builder.query.Query;
import pattern.design.creational.builder.queryBuilder.MongoDBQueryBuilder;
import pattern.design.creational.builder.queryBuilder.QueryBuilder;
import pattern.design.creational.builder.queryBuilder.SqlQueryBuilder;

public class Builder {
    public static void main(String[] args) {

        QueryBuildDirector director = new QueryBuildDirector();

        String from = "<client_table>";
        String where = "<client_name> = <something>";

        QueryBuilder builder = new SqlQueryBuilder();
        Query query = director.buildQuery(from, where, builder);
        query.execute();

        builder = new MongoDBQueryBuilder();
        query = director.buildQuery(from, where, builder);
        query.execute();

    }
}
