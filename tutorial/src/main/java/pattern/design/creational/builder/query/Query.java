package pattern.design.creational.builder.query;

public interface Query {
    void execute();
}
