package pattern.design.creational.builder.queryBuilder;

import pattern.design.creational.builder.query.Query;

public interface QueryBuilder {

    void from(String from);
    void where(String where);
    Query getQuery();
}
