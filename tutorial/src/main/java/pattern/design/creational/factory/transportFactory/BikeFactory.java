package pattern.design.creational.factory.transportFactory;

import pattern.design.creational.factory.transport.Bike;
import pattern.design.creational.factory.transport.Transport;

public class BikeFactory extends TransportFactory {
    @Override
    public Transport create() {
        return new Bike();
    }
}
