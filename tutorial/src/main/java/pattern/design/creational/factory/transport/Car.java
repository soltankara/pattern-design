package pattern.design.creational.factory.transport;

public class Car extends Transport {

    @Override
    public String drive() {
        return "Car driving..";
    }
}
