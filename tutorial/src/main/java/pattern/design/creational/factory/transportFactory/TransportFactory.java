package pattern.design.creational.factory.transportFactory;

import pattern.design.creational.factory.transport.Transport;

public abstract class TransportFactory {
    public abstract Transport create();
}
