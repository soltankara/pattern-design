package pattern.design.creational.factory.transport;

public abstract class Transport {
    public abstract String drive();
}
