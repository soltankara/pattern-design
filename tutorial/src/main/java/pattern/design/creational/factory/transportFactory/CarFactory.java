package pattern.design.creational.factory.transportFactory;

import pattern.design.creational.factory.transport.Car;
import pattern.design.creational.factory.transport.Transport;

public class CarFactory extends TransportFactory {

    @Override
    public Transport create() {
        return new Car();
    }
}
