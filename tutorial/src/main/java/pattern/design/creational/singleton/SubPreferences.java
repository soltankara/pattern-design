package pattern.design.creational.singleton;

public class SubPreferences extends Preferences {

    @Override
    public void helloSingleton() {
        System.out.println("Hi, I'm sub-pattern.design.creational.patterns.singleton");
    }
}
