package pattern.design.creational.singleton;

public class Singleton {
    public static void main(String[] args) {
        Preferences.getInstance().helloSingleton();
        SubPreferences.getInstance().helloSingleton();
    }
}
