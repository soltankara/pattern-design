package pattern.design.structural.adapter;

public class Adapter {
    public static void main(String[] args) {
        Adapter adapter = new Adapter();

        LegacyRectangle legacyRectangle = new LegacyRectangle();
        LegacyRectangleAdapter legacyRectangleAdapter = new LegacyRectangleAdapter(legacyRectangle);
        adapter.calculateRectangleSize(legacyRectangleAdapter);
    }

    public void calculateRectangleSize(Rectangle rectangle) {
        System.out.println("Rectangle size: " + rectangle.determineSize());
    }
}
