package pattern.design.structural.facade;

public class Facade {
    public static void main(String[] args) {

        FinancialSystemFacade facade = new FinancialSystemFacade();
        facade.createInvoice(1000);

    }
}
