package pattern.design.structural.facade;

public class InvoiceCustomerSystem {
    public void createInvoiceForBill(Bill bill) {
        System.out.println("Creating invoice for billw tih amount: " + bill.getAmount());
    }
}
