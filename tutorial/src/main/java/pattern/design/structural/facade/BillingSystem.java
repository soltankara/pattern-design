package pattern.design.structural.facade;

public class BillingSystem {
    public Bill createBill(Integer amount) {
        return new Bill(amount);
    }
}
